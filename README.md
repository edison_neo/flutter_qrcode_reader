# qrcode_reader

A plugin to read QR Codes on Flutter using PlatformView

## Installing

Just add the package to your `pubspec.yaml`

```yaml
dependencies:
  flutter:
    sdk: flutter

  qrcode_reader:
    path: ../qrcode_reader
```

##### iOS Specific

Since we rely on `PlatformView` you need to add the key `io.flutter.embedded_views_preview` with the value `YES` to your app`s `Info.plist`

## Getting Started

Just create a controller, pass it to the widget and call `scan()` to trigger the native camera.

```dart
var _codeReaderController = QRCodeReaderController();

@override
Widget build(BuildContext context) {
  return Scaffold(
    body: Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        Expanded(
          child: QRCodeReader(
            controller: _codeReaderController,
          ),
        ),
        FlatButton(
          child: Text('LER CÓDIGO'),
          onPressed: _scanCodeTap
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            readCode ?? ''
          ),
        )
      ],
    ),
  );
}

void _onScanCodeTap() async {
  //Will return the string read on the QR Code
  //or null if stopped early
  var code = await _codeReaderController.scan();
  setState(() => readCode = code);
}

```

To stop the camera before a code is read you can use `_codeReaderController.stop()`.

If you want to start the camera automatically you can also call `_codeReaderController.scan()` on `initState()`.