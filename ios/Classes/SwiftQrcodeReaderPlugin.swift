import Flutter
import UIKit

public class SwiftQrcodeReaderPlugin: NSObject, FlutterPlugin {
  public static func register(with registrar: FlutterPluginRegistrar) {
    let channel = FlutterMethodChannel(name: "qrcode_reader", binaryMessenger: registrar.messenger())
    let instance = SwiftQrcodeReaderPlugin()
    registrar.addMethodCallDelegate(instance, channel: channel)
    
    let cameraFactory = BarcodeScanViewControllerFactory(registrar: registrar)
    registrar.register(cameraFactory, withId: "plugins.neomode.com.br/qrcode_reader")
  }

  public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
    result("iOS " + UIDevice.current.systemVersion)
  }
}
