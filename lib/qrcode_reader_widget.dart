import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:qrcode_reader/qrcode_reader_controller.dart';

class QRCodeReader extends StatefulWidget {
  final QRCodeReaderController controller;

  QRCodeReader({this.controller});

  @override
  State<StatefulWidget> createState() => _QRCodeReaderState();
}

class _QRCodeReaderState extends State<QRCodeReader> {
  @override
  void initState() {
    widget.controller.addListener(() {
      setState(() {});
    });

    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Opacity(
      opacity: widget.controller.isCameraVisible ? 1.0 : 0.0,
      child: _buildNativeWidget(),
    );
  }

  Widget _buildNativeWidget() {
    if (Platform.isAndroid) {
      return Container();
    }
    else if (Platform.isIOS) {
      return UiKitView(
        viewType: 'plugins.neomode.com.br/qrcode_reader',
        creationParamsCodec: const StandardMessageCodec(),
        onPlatformViewCreated: _onPlatformViewCreated,
      );
    }
    else {
      return Container();
    }
  }

  //Set the id of the view to the controller so we can create the channel connection
  void _onPlatformViewCreated(int id) {
    widget.controller.setViewId(id);
  }
}