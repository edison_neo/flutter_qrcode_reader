import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';

class QRCodeReaderController extends ChangeNotifier {
  //Since we don't have the channel when the object is created we must use a completer here
  Completer<MethodChannel> _channel = Completer<MethodChannel>();
  bool _isCameraVisible = false;

  bool get isCameraVisible {
    return _isCameraVisible;
  }
  set isCameraVisible(bool value) {
    _isCameraVisible = value;
    notifyListeners();
  }

  QRCodeReaderController();

  /// Set the platform channel based on the id of the native view
  void setViewId(int id) {
    final methodChannel = MethodChannel("plugins.neomode.com.br/qrcode_reader_$id");
    _channel.complete(methodChannel);
  }

  /// Start the camera and try to read a QRCode
  ///
  /// Returns the `String` read on the scanned code
  /// or null if the camera was stopped early
  Future<String> scan() async {
    this.isCameraVisible = true;

    //Make sure the controller has a channel set before proceeding
    final MethodChannel channel = await _channel.future;

    var code = await channel.invokeMethod<String>('scan#start');

    this.isCameraVisible = false;

    return code;
  }

  /// Stop the camera
  Future<void> stop() async {
    this.isCameraVisible = false;

    //Make sure the controller has a channel set before proceeding
    final MethodChannel channel = await _channel.future;

    await channel.invokeMethod<String>('scan#stop');
  }
}