import 'dart:async';

import 'package:flutter/services.dart';

class QrcodeReader {
  static const MethodChannel _channel =
      const MethodChannel('qrcode_reader');

  static Future<String> get platformVersion async {
    final String version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }
}
